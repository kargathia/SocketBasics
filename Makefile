ALL     := sbcs_utils sbcs_impl sbcs_test

all: $(ALL)
	@echo 'Build success! \\o/'

clean: TARGET=clean
clean: $(ALL)

cleaner: TARGET=cleaner
cleaner: $(ALL)

test: all
	@cd sbcs_test/bin && ./sbcs_test

demo: all
	@cd Demo/bin && ./Demo

lint:
	@echo 'Running cpplint...' \
 && cpplint --quiet --counting=detailed $(shell find . -name *.h -or -name *.cpp)\
 && echo 'Well done!'

$(ALL):
	@echo "Making ***$@***"
	@cd $@ && make --no-print-directory $(TARGET)

.PHONY: $(ALL) all lint
