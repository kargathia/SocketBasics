#include <log.h>
#include <socket_client.h>
#include <socket_exception.h>
#include <socket_server.h>
#include <unistd.h>
#include <chrono>
#include <cstdlib>
#include <future>
#include <thread>
#include "gtest/gtest.h"

namespace {
int server_port = 9000;  // Will be incremented for each test to avoid testing
                         // on recently used ports

const char *kServerIP = "localhost";

const int kShortTimeoutMs = 100;
const int kPollTimeoutMs = 1 * 1000;
}

TEST(socket_base_test, Test_ServerCDtor) { sbcs::socket_server server; }

TEST(socket_base_test, Test_ClientCDtor) { sbcs::socket_client client; }

TEST(socket_base_test, Test_Server) {
  server_port++;
  sbcs::socket_server socket;
  ASSERT_TRUE(socket.Bind(server_port));
  ASSERT_TRUE(socket.IsValid());

  sbcs::socket_server duplicate;
  ASSERT_FALSE(duplicate.Bind(server_port));
}

TEST(socket_base_test, Test_BlankPoll) {
  server_port++;
  sbcs::socket_server server;
  ASSERT_TRUE(server.Bind(server_port));
  ASSERT_TRUE(server.Poll(kShortTimeoutMs));
}

bool PollSocket(int poll_count) {
  bool result = true;
  sbcs::socket_server server;
  if (!server.Bind(server_port)) {
    return false;
  }
  for (int i = 0; i < poll_count; ++i) {
    result = result && server.Poll(kPollTimeoutMs);
  }

  return result;
}

TEST(socket_base_test, Test_Connect) {
  server_port++;
  std::future<bool> poll_result = std::async(std::launch::async, &PollSocket, 1);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // Give server time to set up
  sbcs::socket_client client;
  ASSERT_TRUE(client.Connect(kServerIP, server_port));
  ASSERT_TRUE(poll_result.get());
}

TEST(socket_base_test, Test_Send) {
  server_port++;
  std::future<bool> poll_result = std::async(std::launch::async, &PollSocket, 2);

  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // Give server time to set up
  sbcs::socket_client client;
  ASSERT_TRUE(client.Connect(kServerIP, server_port));
  ASSERT_TRUE(client.Send("Nobody expects the Spanish Inquisition!"));

  // Append Data
  for (int i = 0; i < 10; ++i) {
    ASSERT_TRUE(client.Send(" " + std::to_string(i)));
  }

  ASSERT_FALSE(client.Send(""));

  std::string largeStr(2048, ' ');
  ASSERT_TRUE(client.Send(largeStr));

  sbcs::socket_client dummy;
  ASSERT_FALSE(dummy.Send("Should have connected first"));

  ASSERT_TRUE(poll_result.get());
}
