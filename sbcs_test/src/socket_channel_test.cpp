#include <log.h>
#include <socket_channel.h>
#include <socket_exception.h>
#include <future>
#include "message_listener_impl.h"
#include "gtest/gtest.h"

namespace {
int local_port = 6000;   // Will be incremented for each test
int remote_port = 6500;  // Will be incremented for each test

const int kTimeoutMs = 1000;
const char *kAddress = "localhost";
}

void Increment() {
  local_port++;
  remote_port++;
}

TEST(socket_channel_test, test_init) {
  Increment();
  sbcs::socket_channel channel(local_port);
  channel.Start();

  // Disabled until #36 (Implement replacement socket_channel promise) is fixed
  // socket_channel duplicate(local_port);
  // ASSERT_THROW(duplicate.Start(), socket_exception);
}

TEST(socket_channel_test, test_send) {
  Increment();
  sbcs::socket_channel local(local_port);
  local.Start();

  std::shared_ptr<message_listener_impl> listener = std::make_shared<message_listener_impl>();

  sbcs::socket_channel remote(remote_port);
  remote.AddListener(listener);
  remote.Start();

  std::string sent_message = "Testing 1 2 3";
  local.Send(sbcs::socket_message(sent_message), kAddress, remote_port);
  std::string second_message = "Testing 5 6";
  local.Send(sbcs::socket_message(second_message), kAddress, remote_port);

  ASSERT_EQ(sent_message, listener->ExpectMessage(kTimeoutMs).GetContent());
  ASSERT_EQ(second_message, listener->ExpectMessage(kTimeoutMs).GetContent());

  std::string largeString(10000, 'a');
  local.Send(sbcs::socket_message(largeString), kAddress, remote_port);
  sbcs::socket_message long_message = listener->ExpectMessage(kTimeoutMs);
  ASSERT_EQ(largeString, long_message.GetContent());

  ASSERT_EQ(long_message.GetSenderAddress().address, "127.0.0.1");
  ASSERT_TRUE(long_message.GetSenderAddress().valid);

  ASSERT_THROW(local.Send(sbcs::socket_message(""), kAddress, remote_port), std::invalid_argument);
}

TEST(socket_channel_test, test_connection) {
  Increment();
  sbcs::socket_channel remote(remote_port);
  std::shared_ptr<message_listener_impl> listener = std::make_shared<message_listener_impl>();
  remote.AddListener(listener);
  remote.Start();

  sbcs::socket_channel local(local_port);
  std::shared_ptr<sbcs::socket_connection> conn = local.OpenConnection("localhost", remote_port);
  std::string first_half = "Row, row, row your boat, ";
  std::string second_half = "gently down the stream";
  conn->Send(first_half);
  conn->Send(second_half);

  std::string reply = listener->ExpectMessage(kTimeoutMs).GetContent();
  ASSERT_EQ(reply, first_half);
  reply = listener->ExpectMessage(kTimeoutMs).GetContent();
  ASSERT_EQ(reply, second_half);
}

TEST(socket_channel_test, test_reply) {
  Increment();
  sbcs::socket_channel remote(remote_port);
  std::shared_ptr<message_listener_impl> listener = std::make_shared<message_listener_impl>();
  remote.AddListener(listener);
  remote.Start();

  std::string reply = "pong";
  listener->QueueReply(reply);

  sbcs::socket_connection conn;
  conn.Connect("localhost", remote_port);
  conn.Send("ping");
  ASSERT_TRUE(conn.Poll(kTimeoutMs));
  ASSERT_EQ(conn.Get(), reply);
}
