#include <SSTR.h>
#include <log.h>
#include <socket_channel.h>
#include <socket_connection.h>
#include <stdexcept>
#include "message_listener_impl.h"
#include "gtest/gtest.h"

class socket_connection_test : public ::testing::Test {
 protected:
  sbcs::socket_connection conn;
  sbcs::socket_channel channel;
  std::shared_ptr<message_listener_impl> listener;
  static const int kPollTimeoutMs = 1000;

  virtual void SetUp() {
    static int serverPort(7500);
    serverPort++;

    listener = std::make_shared<message_listener_impl>();
    listener->SetEchoServer(true);
    channel.SetPort(serverPort);
    channel.AddListener(listener);
    channel.Start();

    conn.Connect("localhost", serverPort);
  }

  virtual void TearDown() {}
};

TEST_F(socket_connection_test, test_echo) {
  conn.Send("hello");

  ASSERT_TRUE(conn.Poll(kPollTimeoutMs));
  ASSERT_EQ(conn.Peek(), "hello");
  ASSERT_EQ(conn.Get(), "hello");

  // We already got the message, so shouldn't be anything left
  ASSERT_FALSE(conn.Poll(kPollTimeoutMs));
  ASSERT_EQ(conn.Peek(), "");
  ASSERT_EQ(conn.Get(), "");
}

TEST_F(socket_connection_test, test_chaining) {
  conn.Send("ha");
  conn.Send("lf");

  ASSERT_TRUE(conn.Poll(kPollTimeoutMs));
  ASSERT_TRUE(conn.Poll(kPollTimeoutMs));
  ASSERT_EQ(conn.Get(), "half");
}
