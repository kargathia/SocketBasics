#pragma once

#include <log.h>
#include <socket_channel.h>
#include <socket_message_listener.h>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <queue>
#include <string>
#include <thread>
#include <vector>

class message_listener_impl : public sbcs::socket_message_listener {
 public:
  inline void OnMessage(const sbcs::socket_address& address, const sbcs::socket_message& message) {
    std::unique_lock<std::mutex> lk(_mtx);
    // LOG(INFO) << "On listener message: " << message.GetContent();
    _received.push_back(message);

    if (_is_echo_server) {
      _channel.Send(address, message);
    } else if (!_replies.empty()) {
      std::string reply = _replies.front();
      _channel.Send(address, sbcs::socket_message(reply));
      // LOG(INFO) << "Replying \"" << _replies.front() << "\" to \"" << message.GetContent() << "\"";
      _replies.pop();
    }

    _cv.notify_one();
  }

  void OnAccept(const sbcs::socket_address& address) {}
  void OnClose(const sbcs::socket_address& address) {}
  void OnError(const sbcs::socket_address& address, const sbcs::socket_exception& ex) {}

  inline sbcs::socket_message ExpectMessage(int timeout_ms) {
    std::unique_lock<std::mutex> lk(_mtx);
    // LOG(INFO) << "Received count: " << _received.size();
    if (_received.empty()) {
      _cv.wait_for(lk, std::chrono::milliseconds(timeout_ms), [this] { return !_received.empty(); });
    }

    if (_received.empty()) {
      throw std::runtime_error("Expect socket_message timed out");
    }

    sbcs::socket_message retVal = _received[0];
    _received.erase(_received.begin());
    return retVal;
  }

  inline void QueueReply(const std::string& new_reply) { _replies.push(new_reply); }
  inline void SetEchoServer(bool new_echo) { _is_echo_server = new_echo; }

 private:
  std::vector<sbcs::socket_message> _received;
  std::mutex _mtx;
  std::condition_variable _cv;

  std::queue<std::string> _replies;
  bool _is_echo_server = false;
  sbcs::socket_channel _channel;
};
