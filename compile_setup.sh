#! /bin/sh

# Install required packages
apt-get -qq update 1>/dev/null
apt-get -qq upgrade 1>/dev/null
apt-get -qq install make autoconf libgtest-dev cmake 1>/dev/null
cd /usr/src/gtest && cmake . && make && mv libg* /usr/lib/ && cd -