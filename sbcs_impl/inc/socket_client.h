#pragma once

#include <netinet/in.h>
#include <string>
#include "socket_address.h"
#include "socket_base.h"

namespace sbcs {
class socket_client : public socket_base {
 public:
  socket_client();
  socket_client(int fdesc, const sockaddr_in& address);
  virtual ~socket_client();

  bool Send(const std::string& message);
  bool Connect(const std::string& address, int port);

  sbcs::socket_address GetPeerName();

 protected:
  int GetRemoteFDesc() { return _remote_fdesc; }

 private:
  void DepleteSendBuffer(int attempts);

 private:
  int _remote_fdesc = -1;
};
}  // namespace sbcs
