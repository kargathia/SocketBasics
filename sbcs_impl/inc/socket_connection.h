#pragma once

#include <sstream>
#include <string>
#include "socket_client.h"
#include "socket_exception.h"

namespace sbcs {
class socket_connection : public socket_client {
 public:
  socket_connection();
  virtual ~socket_connection();

  bool Send(const std::string& message);
  bool Connect(const std::string& address, int port);
  void Disconnect() { socket_base::Disconnect(); }

  bool Poll(int timeout_ms);
  std::string Peek();
  std::string Get();

 private:
  struct pollfd _polled_fd;
  std::stringstream _cache;
};
}  // namespace sbcs
