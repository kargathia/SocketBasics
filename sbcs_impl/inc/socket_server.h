#pragma once

#include <netinet/in.h>
#include <functional>
#include <memory>
#include <string>
#include <vector>
#include "socket_base.h"
#include "socket_message_listener.h"

class pollfd;
typedef std::function<void(int, const std::string&)> MessageHandlerFunc_t;

namespace sbcs {
class socket_server : public socket_base {
 public:
  socket_server();
  virtual ~socket_server();

  bool Bind(int port);
  bool Poll(int timeout_ms);

  void SetMessageHandler(MessageHandlerFunc_t handler) { _message_handler = handler; }

 private:
  int Accept();
  bool Insert(int new_fd);
  void CompressPolledArray();
  void ClosePolledFD(pollfd* polled);

 private:
  struct pollfd _polled_fds[200];
  int _num_polled_fds;
  MessageHandlerFunc_t _message_handler;

 private:
  const int kBacklogSize = 20;
  const int kRetryAttempts = 40;
};
}  // namespace sbcs
