#pragma once

#include <atomic>
#include <condition_variable>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include "socket_address.h"
#include "socket_connection.h"
#include "socket_exception.h"
#include "socket_message.h"
#include "socket_message_listener.h"
#include "socket_server.h"

namespace sbcs {
class socket_channel {
 public:
  explicit socket_channel(int localPort = -1);
  virtual ~socket_channel();

 public:
  /*
  Starts listening on local port for incoming connections and messages.
  If you only want to use socket_channel to send messages, you do not need to call this.
  */
  void Start();

  /*
  Adds a Listener to the channel. If the channel receives a message, its OnMessage() function will be called.
  OnMessage() will only be called by a single thread per channel.
  */
  void AddListener(std::weak_ptr<sbcs::socket_message_listener> listener);

  /*
  Sends a message to the destination address/port, and closes the connection immediately.
  */
  void Send(const sbcs::socket_message& message, const std::string& destination, int destinationPort);

  /*
  Sends a message to a pre-existing connection. Does not close the connection.
  Is not thread-safe with other Send calls on the connection.
  */
  void Send(const sbcs::socket_address& address, const sbcs::socket_message& message);

  /*
  Opens a connection to address/port, and returns the initialized socket_connection object.
  Does not send any messages yet.
  */
  std::shared_ptr<sbcs::socket_connection> OpenConnection(const std::string& destination, int port);

  /*
  Sets port at which it will listen when started.
  Throws std::runtime_error if channel is currently started.
  */
  void SetPort(int port);

  /*
  Toggles use of meta data for server and client sockets.
  Default is true
  */
  void SetMetaDataEnabled(bool enabled);

  /*
  Returns local port, at which it will be listening when Start() is called.
  */
  int GetPort() const { return _port; }

 private:
  void RunServer();
  void RunClient();
  void NotifyListeners();
  void HandleMessage(int fdesc, const std::string& message);
  bool WaitMessage(sbcs::socket_message* message);

 private:
  int _port;
  bool _meta_data_enabled = true;

  std::mutex _listener_queue_mtx;
  std::vector<std::weak_ptr<socket_message_listener>> _queued_listeners;
  std::vector<std::weak_ptr<socket_message_listener>> _listeners;

  std::vector<sbcs::socket_message> _messages;
  std::condition_variable _message_cv;
  std::mutex _message_mtx;

  std::thread _server_thread;
  std::thread _handler_thread;
  std::atomic_bool _is_running;

 private:  // disable copy constructor
  explicit socket_channel(sbcs::socket_channel&);
};
}  // namespace sbcs
