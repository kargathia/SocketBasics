#pragma once

#include <ctime>
#include <string>
#include "socket_address.h"

namespace sbcs {
class socket_message {
 public:
  std::string GetContent() const { return _content; }
  time_t GetTimestamp() const { return _timestamp; }
  const socket_address& GetSenderAddress() const { return _sender_address; }

 public:
  socket_message(const std::string& content, const socket_address& address)
      : _content(content), _sender_address(address), _timestamp(time(0)) {}
  explicit socket_message(const std::string& content) : _content(content), _timestamp(time(0)) {}
  virtual ~socket_message() {}

 private:
  std::string _content;
  socket_address _sender_address;
  time_t _timestamp;
};
}  // namespace sbcs
