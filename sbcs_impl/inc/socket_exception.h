#pragma once

#include <stdexcept>
#include <string>

namespace sbcs {
class socket_exception : public std::exception {
 public:
  /** Constructor (C strings).
     *  @param message C-style string error message.
     *                 The string contents are copied upon construction.
     *                 Hence, responsibility for deleting the char* lies
     *                 with the caller.
     */
  explicit socket_exception(const char *message) : _msg(message) {}

  /** Constructor (C++ STL strings).
     *  @param message The error message.
     */
  explicit socket_exception(const std::string &message) : _msg(message) {}

  /** Destructor.
     * Virtual to allow for subclassing.
     */
  virtual ~socket_exception() throw() {}

  /** Returns a pointer to the (constant) error description.
     *  @return A pointer to a const char*. The underlying memory
     *          is in posession of the Exception object. Callers must
     *          not attempt to free the memory.
     */
  virtual const char *what() const throw() { return _msg.c_str(); }

 protected:
  /** Error message.
     */
  std::string _msg;
};
}  // namespace sbcs
