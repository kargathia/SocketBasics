#pragma once

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <mutex>
#include <string>
#include "socket_address.h"

namespace sbcs {
class socket_base {
 protected:
  socket_base();

 public:
  virtual ~socket_base();

 public:
  static sbcs::socket_address GetPeerName(int fdesc);
  static bool CheckValid(int fdesc);
  static std::string GetErrString(int err);

 public:  // Getters
  bool IsValid() const { return _fdesc != -1; }
  int GetFDesc() const { return _fdesc; }
  const sockaddr_in& GetAddr() const { return _address; }

  void SetMetaDataEnabled(bool enable) { _meta_data_enabled = enable; }
  bool IsMetaDataEnabled() { return _meta_data_enabled; }

 protected:
  void Disconnect();
  bool Configure();
  bool Configure(const sockaddr_in& address);
  bool Configure(const sockaddr_in& address, int fdesc);

  bool SetBlocking(bool blocking);
  void SetAddr(sockaddr_in address) { _address = address; }

  bool Read(int fdesc, std::stringstream* output);
  bool Send(int fdesc, const std::string& message);

 protected:
  const int kOptionOn = 1;
  const int kOptionOff = 0;

 private:
  int Create();

 private:
  int _fdesc;
  sockaddr_in _address;
  bool _meta_data_enabled = true;
  static std::mutex _socket_create_mtx;

 private:
  const int kReceiveTimeoutS = 0;
  const int kSendTimeoutS = 0;
  const int kReceiveTimeoutUs = 50 * 1000;
  const int kSendTimeoutUs = 50 * 1000;

  const int kReadBufferSize = 1024;
  const size_t kMetaDataLength = sizeof(uint32_t);

 private:
  // Copy constructor disabled
  explicit socket_base(socket_base&);
};
}  // namespace sbcs
