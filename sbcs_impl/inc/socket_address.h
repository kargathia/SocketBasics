#pragma once

#include <string>

namespace sbcs {
struct socket_address {
  bool valid = false;
  int fdesc = -1;
  int port = -1;
  std::string address;
};
}  // namespace sbcs
