#pragma once

#include <string>
#include "socket_address.h"
#include "socket_exception.h"
#include "socket_message.h"

namespace sbcs {
class socket_message_listener {
 public:
  virtual ~socket_message_listener() {}

 public:
  virtual void OnMessage(const sbcs::socket_address& address, const socket_message& message) = 0;

  virtual void OnAccept(const sbcs::socket_address& address) = 0;
  virtual void OnClose(const sbcs::socket_address& address) = 0;
  virtual void OnError(const sbcs::socket_address& address, const socket_exception& ex) = 0;

 protected:
  socket_message_listener() {}
};
}  // namespace sbcs
