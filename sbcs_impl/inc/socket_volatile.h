#pragma once

#include <string>
#include "socket_base.h"

namespace sbcs {
/*
Small wrapper for when we want to send to a file descriptor without owning it.
Creating a socket_client/socket_base for a given file descriptor would close it in the destructor.
*/
class socket_volatile : public socket_base {
 public:
  socket_volatile() {}
  virtual ~socket_volatile() {}

  bool Read(int fdesc, std::stringstream* output) { return socket_base::Read(fdesc, output); }
  bool Send(int fdesc, const std::string& message) { return socket_base::Send(fdesc, message); }
};
}  // namespace sbcs
