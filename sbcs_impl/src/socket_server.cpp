#include "socket_server.h"
#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <thread>
#include "socket_client.h"
#include "socket_exception.h"

namespace {}

void DefaultMessageHandler(int fdesc, const std::string &content) {
  LOG(INFO) << "socket_message to default message handler: " << content;
}

sbcs::socket_server::socket_server() : _num_polled_fds(1), _message_handler(DefaultMessageHandler) {
  ::memset(&_polled_fds, 0, sizeof(_polled_fds));
}

sbcs::socket_server::~socket_server() {}

bool sbcs::socket_server::Bind(int port) {
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = ::htons(port);

  if (!Configure(address)) {
    return false;
  }

  if (::setsockopt(GetFDesc(), SOL_SOCKET, SO_REUSEADDR, &kOptionOn, sizeof(kOptionOn)) < 0) {
    LOG(ERROR) << "Unable to turn on address reuse: " << GetErrString(errno);
    return false;
  }

  if (::bind(GetFDesc(), (sockaddr *)&address, sizeof(address)) < 0) {
    LOG(ERROR) << "Error binding: " << GetErrString(errno);
    return false;
  } else {
    SetAddr(address);
  }

  if (::listen(GetFDesc(), kBacklogSize) < 0) {
    LOG(ERROR) << "Error listening: " << GetErrString(errno);
    return false;
  }

  LOG(INFO) << "Successfully bound to port " << port;

  return true;
}

bool sbcs::socket_server::Poll(int timeout_ms) {
  // Ensure that this socket is always polled
  _polled_fds[0].fd = GetFDesc();
  _polled_fds[0].events = POLLIN;

  int result_code = ::poll(_polled_fds, _num_polled_fds, timeout_ms);

  if (result_code < 0) {  // error
    LOG(ERROR) << "Poll failed: " << GetErrString(errno);
    return false;
  }

  if (result_code == 0) {  // timeout
    return true;
  }

  // Check if there are any sockets ready to be accepted
  if (_polled_fds[0].revents > 0) {
    while (true) {
      int new_fd = Accept();
      if (new_fd > 0) {
        _polled_fds[_num_polled_fds].fd = new_fd;
        _polled_fds[_num_polled_fds].events = POLLIN;
        _polled_fds[_num_polled_fds].revents = 0;
        _num_polled_fds++;
      } else {
        break;
      }
    }
  }

  bool needs_compressing = false;
  // Loop through all polls descriptions
  for (int i(1); i < _num_polled_fds; ++i) {
    pollfd *current_poll = &_polled_fds[i];

    if (current_poll->revents == 0) {
      // Nothing to see here. Move along.
      continue;
    }

    if (current_poll->revents & POLLHUP) {
      LOG(INFO) << "FD " << current_poll->fd << " disconnected";
      ClosePolledFD(current_poll);
      needs_compressing = true;
      continue;
    }

    if (current_poll->revents & POLLERR) {
      LOG(INFO) << "FD " << current_poll->fd << " had an error";
      ClosePolledFD(current_poll);
      needs_compressing = true;
      continue;
    }

    if (current_poll->revents & POLLIN) {
      std::stringstream ss;
      if (Read(current_poll->fd, &ss)) {
        std::string message = ss.str();
        _message_handler(current_poll->fd, message);
      } else {
        ClosePolledFD(current_poll);
      }
    }
  }
  if (needs_compressing) {
    CompressPolledArray();
  }
  return true;
}

void sbcs::socket_server::ClosePolledFD(pollfd *polled) {
  ::close(polled->fd);
  polled->fd = -1;
}

int sbcs::socket_server::Accept() {
  if (!CheckValid(GetFDesc())) {
    return false;
  }

  int fdesc = -1;
  struct sockaddr_in client_addr;
  int client_addr_len = sizeof((struct sockaddr *)&client_addr);

  int attempt = 0;
  do {
    attempt++;
    fdesc = ::accept(GetFDesc(), (struct sockaddr *)&client_addr, (socklen_t *)&client_addr_len);
  } while (fdesc < 0 && attempt < kRetryAttempts && errno == EAGAIN);

  if (fdesc < 0 && errno != EAGAIN) {
    LOG(ERROR) << "Failed to accept socket: " << GetErrString(errno);
    return -1;
  }

  if (!CheckValid(fdesc)) {
    return -1;
  }
  socket_address info = GetPeerName(fdesc);
  LOG(INFO) << "Accepted FD " << fdesc << " from " << info.address << ":" << info.port;
  return fdesc;
}

void sbcs::socket_server::CompressPolledArray() {
  for (int i = 0; i < _num_polled_fds; i++) {
    if (_polled_fds[i].fd < 0) {
      for (int j = i; j < _num_polled_fds; j++) {
        _polled_fds[j].fd = _polled_fds[j + 1].fd;
      }
      _num_polled_fds--;
    }
  }
}
