#include "socket_base.h"
#include <SSTR.h>
#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <mutex>
#include <thread>
#include "socket_exception.h"

std::mutex sbcs::socket_base::_socket_create_mtx;

std::string sbcs::socket_base::GetErrString(int err) { return SSTR(strerror(err) << "(" << err << ")"); }

sbcs::socket_base::socket_base() : _fdesc(-1) { ::memset(&_address, 0, sizeof(_address)); }

sbcs::socket_base::~socket_base() { Disconnect(); }

void sbcs::socket_base::Disconnect() {
  if (_fdesc >= 0) {
    int retVal = ::close(_fdesc);
    LOG_IF((retVal < 0), WARNING) << "Failed to disconnect " << _fdesc << ": " << GetErrString(errno);
  }
  _fdesc = -1;
}

bool sbcs::socket_base::CheckValid(int fdesc) { return (fcntl(fdesc, F_GETFL) != -1 || errno != EBADF); }

int sbcs::socket_base::Create() {
  std::lock_guard<std::mutex> lock(_socket_create_mtx);
  return ::socket(AF_INET, SOCK_STREAM, 0);
}

bool sbcs::socket_base::Configure() {
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = 0;

  return Configure(address);
}

bool sbcs::socket_base::Configure(const sockaddr_in &address) { return Configure(address, Create()); }

bool sbcs::socket_base::Configure(const sockaddr_in &address, int fdesc) {
  if (!CheckValid(fdesc)) {
    return false;
  }

  _fdesc = fdesc;
  _address.sin_family = address.sin_family;
  _address.sin_addr.s_addr = address.sin_addr.s_addr;
  _address.sin_port = address.sin_port;

  struct timeval tv;

  tv.tv_usec = kReceiveTimeoutUs;
  tv.tv_sec = kReceiveTimeoutS;
  if (::setsockopt(_fdesc, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
    LOG(ERROR) << "Unable to set receive timeout: " << GetErrString(errno);
    return false;
  }

  tv.tv_usec = kSendTimeoutUs;
  tv.tv_sec = kSendTimeoutS;
  if (::setsockopt(_fdesc, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0) {
    LOG(ERROR) << "Unable to set send timeout: " << GetErrString(errno);
    return false;
  }

  struct sigaction sa;
  sa.sa_handler = SIG_IGN;
  sa.sa_flags = 0;
  if (sigaction(SIGPIPE, &sa, 0) == -1) {
    LOG(ERROR) << "Unable to ignore SIGPIPE: " << GetErrString(errno);
    return false;
  }

  if (!SetBlocking(false)) {
    return false;
  }

  return true;
}

bool sbcs::socket_base::SetBlocking(bool blocking) {
  int option_val = blocking ? kOptionOff : kOptionOn;
  int result_code = ioctl(_fdesc, (int)FIONBIO, (char *)&option_val);

  if (result_code < 0) {
    LOG(ERROR) << "Unable to set blocking: " << GetErrString(errno);
  }

  return result_code >= 0;
}

sbcs::socket_address sbcs::socket_base::GetPeerName(int fdesc) {
  socklen_t len;
  struct sockaddr_storage addr;
  char ipstr[INET6_ADDRSTRLEN];
  int port;

  sbcs::socket_address info;

  len = sizeof addr;
  if (::getpeername(fdesc, (struct sockaddr *)&addr, &len) < 0) {
    info.valid = false;
    info.address = "Invalid address";
    info.port = -1;
    return info;
  }

  // deal with both IPv4 and IPv6:
  if (addr.ss_family == AF_INET) {
    struct sockaddr_in *s = (struct sockaddr_in *)&addr;
    port = ntohs(s->sin_port);
    inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
  } else {  // AF_INET6
    struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
    port = ntohs(s->sin6_port);
    inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
  }

  info.fdesc = fdesc;
  info.address = ipstr;
  info.port = port;
  info.valid = true;

  return info;
}

bool sbcs::socket_base::Read(int fdesc, std::stringstream *output) {
  int status = 0;
  char buffer[kReadBufferSize];
  uint32_t max_read = kReadBufferSize - 1;
  uint32_t length = 0;
  size_t received = 0;

  if (IsMetaDataEnabled()) {
    char meta_buffer[kMetaDataLength];
    status = ::recv(fdesc, meta_buffer, kMetaDataLength, 0);
    memcpy(&length, meta_buffer, kMetaDataLength);
  } else {
    status = 1;
  }

  while (status > 0) {
    // receive message size or buffer size - whichever is smaller
    uint32_t remaining = length - received;
    uint32_t wanted_bytes = (remaining > max_read || !IsMetaDataEnabled()) ? max_read : remaining;

    // avoids buffer contamination by prev message blocks
    memset(&buffer, 0, sizeof(buffer));
    status = ::recv(fdesc, buffer, wanted_bytes, 0);

    if (buffer[0] != 0) {
      *output << buffer;
      received += strlen(buffer);
    }
    if (IsMetaDataEnabled() && received >= length) {
      break;
    }
  }

  if (status < 0 && errno != EAGAIN) {
    LOG(ERROR) << "Receive error: " << GetErrString(errno);
  }

  return received > 0;
}

bool sbcs::socket_base::Send(int fdesc, const std::string &message) {
  if (message.empty()) {
    return false;
  }
  size_t length = message.size();
  size_t sent = 0;
  const char *data = message.c_str();
  int status = 0;

  if (IsMetaDataEnabled()) {
    uint32_t message_size = length;  // hard set to 32 bit to remain portable
    status = ::send(fdesc, &message_size, sizeof(message_size), MSG_NOSIGNAL);
  } else {
    status = 1;
  }

  while (sent < length && status > 0) {
    status = ::send(fdesc, &data[sent], length - sent, MSG_NOSIGNAL | MSG_DONTWAIT);

    if (status < 0) {
      LOG(DEBUG) << "send() error: " << GetErrString(errno);
      break;
    }
    sent += status;
  }

  return (sent == length);
}
