#include <log.h>
#include <stringsplit.h>
#include <unistd.h>
#include <iostream>
#include <memory>
#include <thread>
#include "socket_client.h"
#include "socket_exception.h"
#include "socket_message.h"
#include "socket_message_listener.h"
#include "socket_server.h"
#include "socket_volatile.h"

#include "socket_channel.h"

namespace {
const int POLL_TIMEOUT_MS = 200;
}

sbcs::socket_channel::socket_channel(int local_port) : _port(local_port), _is_running(false) {}

sbcs::socket_channel::~socket_channel() {
  _is_running = false;
  _message_cv.notify_all();

  if (_server_thread.joinable()) {
    _server_thread.join();
  }
  if (_handler_thread.joinable()) {
    _handler_thread.join();
  }
}

void sbcs::socket_channel::SetPort(int port) {
  if (_is_running) {
    throw std::runtime_error("Port cannot be changed while channel is running");
  }
  _port = port;
}

void sbcs::socket_channel::SetMetaDataEnabled(bool enabled) {
  if (_is_running) {
    throw std::runtime_error("Unable to change meta data usage while running");
  }
  _meta_data_enabled = enabled;
}

void sbcs::socket_channel::Start() {
  if (_is_running) {
    throw std::runtime_error("Channel is already running");
  }
  if (_port < 0) {
    throw std::invalid_argument("Invalid port");
  }
  _is_running = true;
  _server_thread = std::thread(&sbcs::socket_channel::RunServer, this);
  _handler_thread = std::thread(&sbcs::socket_channel::RunClient, this);
  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // server needs time to bind
}

void sbcs::socket_channel::RunServer() {
  socket_server server;
  server.SetMetaDataEnabled(_meta_data_enabled);
  using std::placeholders::_1;
  using std::placeholders::_2;
  server.SetMessageHandler(std::bind(&sbcs::socket_channel::HandleMessage, this, _1, _2));
  bool boundOk = server.Bind(_port);
  while (boundOk && _is_running) {
    server.Poll(POLL_TIMEOUT_MS);
  }
}

void sbcs::socket_channel::RunClient() {
  try {
    while (_is_running) {
      NotifyListeners();
    }
  } catch (std::exception &ex) {
    LOG(ERROR) << "socket_channel client thread terminated with error: " << ex.what();
  }
}

bool sbcs::socket_channel::WaitMessage(sbcs::socket_message *message) {
  std::unique_lock<std::mutex> lock(_message_mtx);
  if (_is_running && _messages.empty()) {
    _message_cv.wait(lock, [this] { return !_is_running || !_messages.empty(); });
  }

  if (_is_running && !_messages.empty()) {
    *message = _messages[0];
    _messages.erase(_messages.begin());
    return true;
  }
  return false;
}

void sbcs::socket_channel::NotifyListeners() {
  sbcs::socket_message message("invalid");
  bool message_received = WaitMessage(&message);

  if (_is_running && !_queued_listeners.empty()) {
    // We want to avoid modifying the listeners while they are being notified
    // But it is perfectly legal for functions called by OnMessage() to add a
    // listener
    // This way deadlocks are avoided, and the channel clearly guarantees that
    // the
    // new listener will not be called for the current message
    std::unique_lock<std::mutex> lock(_listener_queue_mtx);
    _listeners.insert(_listeners.end(), _queued_listeners.begin(), _queued_listeners.end());
    _queued_listeners.clear();
  }

  if (!_is_running || !message_received) {
    return;
  }

  std::vector<size_t> erased_elements;

  for (auto rit = _listeners.rbegin(); rit != _listeners.rend(); ++rit) {
    // Because we are using weak pointers, we need to check if they expired
    // before we used them
    if (std::shared_ptr<socket_message_listener> listener_ptr = (*rit).lock()) {
      try {
        listener_ptr->OnMessage(message.GetSenderAddress(), message);
      } catch (std::exception &ex) {
        LOG(ERROR) << "Exception caught in " << __func__ << ": " << ex.what();
      }
    } else {
      erased_elements.push_back(std::distance(begin(_listeners), rit.base()) - 1);
    }
  }

  for (auto idx : erased_elements) {
    _listeners.erase(_listeners.begin() + idx);
  }
}

void sbcs::socket_channel::AddListener(std::weak_ptr<sbcs::socket_message_listener> listener) {
  std::unique_lock<std::mutex> lock(_listener_queue_mtx);
  _queued_listeners.push_back(listener);
}

void sbcs::socket_channel::HandleMessage(int fdesc, const std::string &message) {
  {  // We want to notify outside our lock
    std::unique_lock<std::mutex> lock(_message_mtx);
    _messages.push_back(socket_message(message, socket_base::GetPeerName(fdesc)));
  }
  _message_cv.notify_one();
}

void sbcs::socket_channel::Send(const sbcs::socket_message &message, const std::string &destination, int port) {
  if (message.GetContent().empty()) {
    throw std::invalid_argument("socket_message content was empty");
  }

  socket_connection conn;
  conn.SetMetaDataEnabled(_meta_data_enabled);
  conn.Connect(destination, port);
  conn.Send(message.GetContent());
}

void sbcs::socket_channel::Send(const sbcs::socket_address &address, const sbcs::socket_message &message) {
  if (message.GetContent().empty()) {
    throw std::invalid_argument("socket_message content was empty");
  }

  if (!address.valid) {
    throw std::invalid_argument("socket address invalid");
  }

  socket_volatile sender;
  sender.SetMetaDataEnabled(_meta_data_enabled);
  sender.Send(address.fdesc, message.GetContent());
}

std::shared_ptr<sbcs::socket_connection> sbcs::socket_channel::OpenConnection(const std::string &destination,
                                                                              int port) {
  std::shared_ptr<socket_connection> conn = std::make_shared<socket_connection>();
  conn->SetMetaDataEnabled(_meta_data_enabled);
  conn->Connect(destination, port);
  return conn;
}
