#include "socket_client.h"
#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <chrono>
#include <mutex>
#include <thread>
#include "socket_exception.h"

sbcs::socket_client::socket_client() { Configure(); }

sbcs::socket_client::socket_client(int fdesc, const sockaddr_in& address) { Configure(address, fdesc); }

sbcs::socket_client::~socket_client() { DepleteSendBuffer(2); }

sbcs::socket_address sbcs::socket_client::GetPeerName() { return socket_base::GetPeerName(_remote_fdesc); }

bool sbcs::socket_client::Connect(const std::string& address, int port) {
  if (!CheckValid(GetFDesc())) {
    return false;
  }

  if (!SetBlocking(true)) {
    LOG(DEBUG) << "Unable to set blocking";
    return false;
  }

  int status = 0;
  struct addrinfo hints;
  struct addrinfo* server_address;

  // first, load up address structs with getaddrinfo():
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  // protocol should be represented as a const char*
  // We'll convert port to std::string and then to const char*
  status = ::getaddrinfo(address.c_str(), std::to_string(port).c_str(), &hints, &server_address);

  if (status < 0) {
    LOG(ERROR) << "Getting address info for " << address << ":" << port << " failed: " << GetErrString(errno);
    return false;
  }

  _remote_fdesc = ::connect(GetFDesc(), server_address->ai_addr, server_address->ai_addrlen);
  status = _remote_fdesc;

  if (status < 0) {
    LOG(ERROR) << "Connecting to " << address << ":" << port << " failed: " << GetErrString(errno);
    return false;
  }
  return status >= 0;
}

bool sbcs::socket_client::Send(const std::string& message) {
  if (!SetBlocking(true)) {
    return false;
  }
  return socket_base::Send(GetFDesc(), message);
}

void sbcs::socket_client::DepleteSendBuffer(int attempts) {
  int fd = GetFDesc();
  for (int i = 0; i < attempts; ++i) {
    int outstanding;
    ::ioctl(fd, TIOCOUTQ, &outstanding);
    if (!outstanding) {
      break;
    }

    usleep(100);
  }
}
