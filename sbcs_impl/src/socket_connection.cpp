#include "socket_connection.h"
#include <SSTR.h>
#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <poll.h>
#include <stdio.h>
#include <string>
#include "socket_message.h"

sbcs::socket_connection::socket_connection() {}
sbcs::socket_connection::~socket_connection() {}

bool sbcs::socket_connection::Connect(const std::string& address, int port) {
  if (!socket_client::Connect(address, port)) {
    std::string error = "Unable to connect to " + address + ":" + std::to_string(port);
    throw socket_exception(error);
  }
  return true;
}

bool sbcs::socket_connection::Send(const std::string& message) {
  if (!socket_client::Send(message)) {
    socket_address info = GetPeerName();
    throw socket_exception(SSTR("Unable to send message to " << info.address << ":" << info.port));
  }
  return true;
}

bool sbcs::socket_connection::Poll(int timeout_ms) {
  if (!IsValid()) {
    throw std::invalid_argument("Local socket invalid");
  }

  _polled_fd.fd = GetFDesc();
  _polled_fd.events = POLLIN;
  _polled_fd.revents = 0;

  int result_code = ::poll(&_polled_fd, 1, timeout_ms);

  if (result_code < 0) {  // error
    std::string err = "Poll failed: " + GetErrString(errno);
    LOG(ERROR) << err;
    Disconnect();
    return false;
  }

  if (result_code == 0) {  // timeout
    return false;
  }

  if (_polled_fd.revents == 0) {
    // Nothing to see here. Move along.
    return false;
  }

  if (_polled_fd.revents & POLLERR) {
    LOG(INFO) << "FD " << _polled_fd.fd << " had an error";
    Disconnect();
    throw std::runtime_error("Polled socket had an error");
  }

  if (_polled_fd.revents & POLLNVAL) {
    Disconnect();
    throw std::runtime_error("Invalid file descriptor");
  }

  bool retval = false;
  if (_polled_fd.revents & POLLIN) {
    retval = Read(_polled_fd.fd, &_cache);
  }

  if (_polled_fd.revents & POLLHUP) {
    Disconnect();
    retval = retval || false;
  }

  return retval;
}

std::string sbcs::socket_connection::Peek() { return _cache.str(); }

std::string sbcs::socket_connection::Get() {
  std::string output = _cache.str();
  if (!output.empty()) {
    _cache.clear();
    _cache.str(std::string());
  }
  return output;
}
