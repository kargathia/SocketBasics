#pragma once

#include <algorithm>
#include <string>
#include <vector>

class InputParser {
 public:
  InputParser(int &argc, char **argv) {
    for (int i = 1; i < argc; ++i) this->tokens.push_back(std::string(argv[i]));
  }

  /// @author iain
  std::string getCmdOption(const std::string &option) const {
    std::vector<std::string>::const_iterator itr;
    itr = std::find(this->tokens.begin(), this->tokens.end(), option);
    if (itr != this->tokens.end() && ++itr != this->tokens.end()) {
      return *itr;
    }
    return "";
  }

  /// @author iain
  bool cmdOptionExists(const std::string &option) const {
    return std::find(this->tokens.begin(), this->tokens.end(), option) != this->tokens.end();
  }

 private:
  std::vector<std::string> tokens;
};

// Example

// int main(int argc, char **argv){
//     InputParser input(argc, argv);
//     if(input.cmdOptionExists("-h")){
//         // Do stuff
//     }
//     std::string filename = input.getCmdOption("-f");
//     if (!filename.empty()){
//         // Do interesting things ...
//     }
//     return 0;
// }
